#!/bin/bash

if [ -n "$build_folder" ]; then echo "all good brah"; else build_folder="build"; fi
if [ -n "$region" ]; then echo "all good brah"; else region="eu-central-1"; fi
if [ -n "$policy" ]; then echo "all good brah"; else policy="public"; fi

bucket_name=$repo-$branch

# bucket policy options add new if needed
public='{"Version": "2012-10-17","Statement": [{"Sid": "AddPerm","Effect": "Allow","Principal": "*","Action": "s3:GetObject","Resource": "arn:aws:s3:::BUCKETNAME/*"}]}';

# Disable exit if delete bucket command throws error...this command will throw an error if there is no bucket on s3 and we don't won't to stop execution.
set +e

#delete bucket if exists
#aws s3 rb s3://$bucket_name --force
aws s3 rm s3://bucket-name --recursive

# Reset error handling
set -e

#create new bucket
aws s3api create-bucket --bucket $bucket_name --region $region --create-bucket-configuration LocationConstraint=$region --acl public-read

# set bucket as static website
aws s3 website s3://$bucket_name --index-document index.html --error-document index.html

# set bucket policy
echo "${!policy}" > policy.json;
sed -i "s/BUCKETNAME/$bucket_name/g" policy.json
aws s3api put-bucket-policy --bucket $bucket_name  --policy file://policy.json

# sync files from build folder
aws s3 --region $region sync $build_folder/ s3://$bucket_name --cache-control 'public, max-age=14400' --delete