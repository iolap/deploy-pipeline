#!/bin/bash

if [ -n "$build_folder" ]; then echo "Artefacts path: $build_folder" && ls -l $build_folder; else build_folder="build"; fi
if [ -n "$region" ]; then echo "Region: $region"; else region="eu-central-1"; fi

bucket_name=$repo-$branch

# sync files from build folder
aws s3 --region $region sync $build_folder/ s3://$bucket_name --cache-control 'public, max-age=14400' --delete
